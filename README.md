# What's This?

This project is a small boilerplate / playground for frontend web with React.  Also tries to serve as an example and explanation of the tech stack behind it. Good for branching from if avoiding CRA.

# Actions

- `yarn install` Install all dependencies.
- `yarn lint` - Lint the project
- `yarn fix` - Lint and autofix
- `yarn test` - Run jest tests
    - generates a coverage report to `/reports/coverage`
- `yarn build-dev` - Build to `/dist` with dev webpack settings
    - Generates a webpack analyzer report in `/reports`
- `yarn build` - Build to `/dist` with prod webpack settings
    - Automatic favicon generation
    - Generates a webpack analyzer report in `/reports`
- `yarn launch` - Starts `webpack-dev-server` and serves content to localhost:8080

# Things this does not has yet:

- styled components instead of scss https://styled-components.com/
- redux for global state management https://react-redux.js.org/
- react-helmet for proper metadata setting https://www.npmjs.com/package/react-helmet
- any nice to haves from here: https://github.com/thedaviddias/Front-End-Checklist

# Dependencies

## Website

| Dependency        | Description           | Dev-dependency?  |
| ------------- |-------------| :-----:|
| @types/core-js    | TS type declarations for `core-js` | X
| @types/node   | TS type declarations for `node` | X
| @types/react  | TS type declarations for `react` | X
| @types/react-dom  | TS type declarations for `react-dom` | X
| @types/react-router-dom   | TS type declarations for `react-router-dom` | X
| [bignumber.js](https://www.npmjs.com/package/bignumber.js)  | For precise math with floating number values | 
| [core-js](https://www.npmjs.com/package/core-js)   | Polyfills for newer ES features; must be imported first at entry point | 
| [reset-css](https://www.npmjs.com/package/reset-css) | Used to create basis for element styles that is not informed by browser-specific style rules. | 
| [typescript](https://www.npmjs.com/package/typescript)    | Typescript.  Used for typing as babel handles what would usually be `tsc` compilation | X
| [react](https://www.npmjs.com/package/react) | Logic for react components | 
| [react-dom](https://www.npmjs.com/package/react-dom) | Entrypoint for web rendering of react | 
| [react-router-dom](https://reacttraining.com/react-router/)  | URL routing for react single-page apps | 

## Linting

| Dependency        | Description           | Dev-dependency?  |
| ------------- |-------------| :-----:|
| [eslint@6.x.x](https://eslint.org/) | Linter for TS/JS.  `@6.x.x` as a dependency for `eslint-config-airbnb` | X
| [eslint-config-airbnb](https://www.npmjs.com/package/eslint-config-airbnb)  | Airbnb's eslint config for react and js/ts.  Read instructions to understand proper setup of peer dependencies. | X
| [eslint-plugin-import](https://www.npmjs.com/package/eslint-plugin-import)  | eslint rules for import/export syntax | X
| [eslint-plugin-react](https://www.npmjs.com/package/eslint-plugin-react) | eslint rules for React, peer dependency for airbnb config | X
| [eslint-import-resolver-alias](https://www.npmjs.com/package/eslint-import-resolver-alias) | Required for linting alias imports (like "@") | X
| [@typescript-eslint/parser](https://www.npmjs.com/package/@typescript-eslint/parser) | eslint parser for Typescript | X
| [@typescript-eslint/eslint-plugin](https://www.npmjs.com/package/@typescript-eslint/eslint-plugin)  | peer dependency of the above.  Required to be the **same version**. | X
| [eslint-plugin-jsx-a11y@^6.2.3](https://www.npmjs.com/package/eslint-plugin-jsx-a11y) | peer dependency of eslint-config-airbnb for accessibility rules linting | X
| [eslint-plugin-react-hooks@^2.5.0](https://www.npmjs.com/package/eslint-plugin-react-hooks)   | peer dependency of eslint-config-airbnb for react hooks feature | X

## Testing

| Dependency        | Description           | Dev-dependency?  |
| ------------- |-------------| :-----:|
| [webpack-dev-server](https://webpack.js.org/configuration/dev-server/) | convenient development server for serving the website with hot reload | X
| [jest](https://jestjs.io/docs/en/getting-started)  | JS/TS Testing framework | X
| @types/jest | Typescript declarations for `jest` | X
| [babel-jest](https://www.npmjs.com/package/babel-jest) | Lets jest be used through `babel` automatically via cli | X
| [@testing-library/react](https://testing-library.com/docs/react-testing-library/intro) | convenient API for testing React components.  See: https://testing-library.com/docs/react-testing-library/example-intro | X
| [@testing-library/jest-dom](https://testing-library.com/docs/ecosystem-jest-dom) | helper functions for react testing library for querying the DOM easily | X
| [eslint-plugin-jest](https://www.npmjs.com/package/eslint-plugin-jest) | eslint plugin for proper linting of test files and `jest` functions | X

## Transpiling

| Dependency        | Description           | Dev-dependency?  |
| ------------- |-------------| :-----:|
| [@babel/core](https://babeljs.io/docs/en/babel-core)   | Babel's purpose is to transform modern javascript to ES5-compatible code for use in older environments | X
| [@babel/preset-env](https://babeljs.io/docs/en/babel-preset-env) | Includes several useful plugins for code transformation without configuration required and respects browserlist. | X
| [@babel/preset-typescript](https://babeljs.io/docs/en/babel-preset-typescript)  | Required for typescript babel use instead of tsc compilation | X
| [@babel/preset-react](https://babeljs.io/docs/en/babel-preset-react)   | Required for React/JSX | X
| [@babel/plugin-proposal-optional-chaining](https://babeljs.io/docs/en/babel-plugin-proposal-optional-chaining)  | Required for transforming optional-chaining syntax during build (e.g. `foo.bar?.baz`) | X
| [@babel/plugin-proposal-object-rest-spread](https://babeljs.io/docs/en/babel-plugin-proposal-object-rest-spread) | `const {x, ...y} = {x: 100, a: 200, b: 300}` | X
| [@babel/plugin-syntax-dynamic-import](https://babeljs.io/docs/en/next/babel-plugin-syntax-dynamic-import.html)   | For parsing dynamic importing in code.  Requires manual importing of promise and iterator polyfills from core-js@3 (see link). | X
| [@babel/plugin-transform-runtime](https://babeljs.io/docs/en/babel-plugin-transform-runtime)   | Efficient code re-use for babel helping methods | X
| [@babel/plugin-proposal-class-properties](https://www.npmjs.com/package/@babel/plugin-proposal-class-properties)  | Transformation of static properties of classes and initializer syntax | X
| [@babel/plugin-syntax-import-meta](https://www.npmjs.com/package/@babel/plugin-syntax-import-meta)  | https://github.com/tc39/proposal-import-meta | X
| [@babel/runtime-corejs3](https://www.npmjs.com/package/@babel/runtime-corejs3)    | Helpers for `core.js@3` polyfills when using `@babel/plugin-transform-runtime` with the `corejs: 3` option and using `core.js@3` at code entry. (See plugin transform runtime options) | X

## Bundling

| Dependency        | Description           | Dev-dependency?  |
| ------------- |-------------| :-----:|
| [webpack](https://www.npmjs.com/package/webpack)     | Core tool for module bundling our TS/JS code for the browser | X |
| [webpack-cli](https://www.npmjs.com/package/webpack-cli)     | Required for webpack commands      |   X |
| [webpack-bundle-analyzer](https://www.npmjs.com/package/webpack-bundle-analyzer) | Creates an interactive map of webpack output     |    X |
| [webpack-merge](https://www.npmjs.com/package/webpack-merge) | Used to split webpack configuration into multiple files and then merge them together for common / dev / prod contexts | X
| [babel-loader](https://www.npmjs.com/package/babel-loader/v/8.0.0-beta.1) | Allows webpack to use babel to load source | X
| [css-loader](https://www.npmjs.com/package/css-loader)    | Used by webpack to resolve css imports when bundling | X
| [file-loader](https://www.npmjs.com/package/file-loader)   | Used by webpack to resolve file imports when bundling (png, svg, pdf, etc) | X
| [sass-loader](https://www.npmjs.com/package/sass-loader)   | Allows babel to load sass/scss source, requires `node-sass` and should be used first for loading sass/scss styles from source in webpack before `css-loader` | X
| [style-loader](https://www.npmjs.com/package/style-loader)  | Used for dev building (mini-css-extract-plugin is used for prod) injection of css and compiled sass/scss during bundling | X
| [node-sass](https://www.npmjs.com/package/node-sass) | sass/scss compiling via Node | X
| [friendly-errors-webpack-plugin](https://www.npmjs.com/package/friendly-errors-webpack-plugin)   | Makes webpack errors easier to read | X
| [html-webpack-plugin](https://www.npmjs.com/package/html-webpack-plugin)   | Plugin for webpack to easily create proper html files for serving from base templates | X
| [mini-css-extract-plugin](https://www.npmjs.com/package/mini-css-extract-plugin)  | Extracts built css from webpack and stores them in separate files to load on-demand | X
| [favicons-webpack-plugin](https://www.npmjs.com/package/favicons-webpack-plugin) | Plugin for webpack to automatically generate favicons from an image during bundling | X
| [optimize-css-assets-webpack-plugin](https://www.npmjs.com/package/optimize-css-assets-webpack-plugin) | Does exactly what it says | X
| [clean-webpack-plugin](https://www.npmjs.com/package/clean-webpack-plugin)  | Plugin for webpack to clean the `dist` folder before building to it | X
| [cross-env](https://www.npmjs.com/package/cross-env) | Used to set environment variables such as `NODE_ENV` in yarn scripts in a cross-platform way | X

# Project Workflow

    Typescript + React Website Code             # Website Code
    -> Babel (via `babel-loader` in `webpack`)  # "Transpiling"
    -> Webpack                                  # "Bundling"
    -> `/dist`                                  # Output

The project is written in Typescript with React.  When ready to build, webpack runs through codebase at the given `entry` and treats each imported file via its defined `loader` for its extension.  js/ts/jsx/tsx files are given to `babel-loader` where `babel` handles any code transformations it needs to make.  Files are given to `file-loader` to be properly named and placed in `/dist`.  Etc.  

# Configuration

### Typescript

- `tsconfig.json`

### Babel

- `.babelrc`
- `.browserslistrc`

### Webpack

- `webpack.config.js`
- `webpack/**/*`

### eslint

- `.eslintrc.js`
- `.eslintignore`

### Jest
- `jest.config.js`