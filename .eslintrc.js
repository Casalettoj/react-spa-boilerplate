const path = require('path');

// https://github.com/typescript-eslint/typescript-eslint/blob/master/docs/getting-started/linting/README.md
module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    jest: true,
  },
  parser: '@typescript-eslint/parser',
  plugins: ['jest', '@typescript-eslint'],
  extends: ['airbnb', 'plugin:@typescript-eslint/recommended'],
  rules: {
    'no-unused-vars': 'off',
    'import/no-extraneous-dependencies': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    'react/jsx-filename-extension': [1, { extensions: ['.jsx', '.tsx', '.ts', '.js'] }],
    'import/extensions': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'import/no-unresolved': [2, { caseSensitive: false }],
    'no-use-before-define': 'off',
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@', path.resolve(__dirname, 'src')],
        ],
        extensions: ['.ts', '.js', '.tsx', '.json', '.js'],
      },
    },
  },
};
