const merge = require('webpack-merge');
const webpack = require('webpack');

const paths = require('./paths');
const commonConfig = require('./webpack.common');

module.exports = merge.merge(commonConfig, {
    mode: 'development',
    devtool: 'inline-cheap-module-source-map',
    output: {
      path: paths.build,
      publicPath: "/",
      filename: 'scripts/[hash].bundle.js',
      chunkFilename: 'scripts/[id].[hash].chunk.js'
    },  
    // https://webpack.js.org/configuration/optimization/
    optimization: {
        runtimeChunk: 'single',
        // https://webpack.js.org/plugins/split-chunks-plugin/
        splitChunks: {
            chunks: 'all'
        }
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
        compress: true,
        contentBase: paths.build,
        publicPath: "/",
        historyApiFallback: true,
        hot: true,
        port: 8080,
    }
})