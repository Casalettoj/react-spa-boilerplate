// https://babeljs.io/docs/en/babel-polyfill
import 'core-js/stable';
import 'regenerator-runtime/runtime';
// https://babeljs.io/docs/en/next/babel-plugin-syntax-dynamic-import.html#working-with-webpack-and-babel-preset-env
import 'core-js/modules/es.promise';
import 'core-js/modules/es.array.iterator';
// https://meyerweb.com/eric/tools/css/reset/
import 'reset-css';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import routes from './constants/routes';
import Home from './components/Home';
import Away from './components/Away';

import '@/style/base.scss';

// Polyfill test import
import '@/esnext';

const HomePage = (): JSX.Element => <Home message="This is the home page." />;
const AwayPage = (): JSX.Element => <Away message="This is the not home page." />;

function App(): JSX.Element {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={routes.home} component={HomePage} />
        <Route path={routes.away} component={AwayPage} />
      </Switch>
    </BrowserRouter>
  );
}

ReactDOM.render(<App />, document.getElementById('app'));
