import React from 'react';
import { Link } from 'react-router-dom';

import routes from '@/constants/routes';
import testsvg from '@/images/test.svg';
import testpng from '@/images/chao.png';

export interface HomeProps {
    message: string;
}

export default function Home(props: HomeProps): JSX.Element {
  const { message }: { message: string } = props;
  return (
    <div>
      {message}
      <br />
      <img src={testsvg} alt="" />
      <br />
      <img src={testpng} alt="" />
      <br />
      <Link to={routes.away}>Go Away</Link>
    </div>
  );
}
