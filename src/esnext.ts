/* eslint-disable no-console */
console.log('Hello World!');

// array.prototype.includes() is an ES7 feature.
// Showing its transpiling and polyfill along with arrow functions via the IIFE:
((): void => {
  const numbers = [1, 2, 3, 4];
  if (numbers.includes(3, 1)) {
    console.log('ES7 working.');
  }

  // Showing object-spread and optional chaining
  const obj: {x: number; y: number; a: number; b: number; c: number; d?: {a: number}} = {
    x: 100, y: 200, a: 300, b: 400, c: 500,
  };
  const { x, y, ...z } = obj;
  console.log(x); // 100
  console.log(y); // 200
  console.log(z); // { a: 300, b: 400, c: 500 }
  console.log(obj.d?.a); // undefined
  obj.d = { a: 1 };
  console.log(obj.d?.a); // 1
})();
